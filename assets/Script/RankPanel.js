
const GameSdkMan = require("GameSdkMan");


cc.Class({
    extends: cc.Component,

    properties: {
        rank: cc.Sprite,

        friendRankTitle: cc.Sprite,
        groupRankTitle: cc.Sprite
    },

    onLoad() {
        this.rankType = 'friend';
    },
    start() {
        this.initView();
    },
    update() {
        this.rank.spriteFrame = GameSdkMan.getSharedCanvasSpriteFrame();
    },
    onDestroy() {
        GameSdkMan.postMsg({message: 'hideAllPanels'});
    },

    initView() {
        if (this.rankType == 'friend') {
            this.friendRankTitle.node.active = true;
            this.groupRankTitle.node.active = false;

            GameSdkMan.postMsg({message: 'friendRanking'});
        } else if (this.rankType == 'group') {
            this.groupRankTitle.node.active = true;
            this.friendRankTitle.node.active = false;

            GameSdkMan.postMsg({message: 'groupRanking', shareTicket: GameSdkMan.shareTicket});
        }
    }
});
