
const GameSdkMan = require("GameSdkMan");
const i18n = require('i18n');


cc.Class({
    extends: cc.Component,

    properties: {
        musicToggle: cc.Toggle,

        shareCount: 3
    },

    onLoad() {
        GameSdkMan.init();

        GameSdkMan.setShareMenuOptions({
            title: i18n.t('shareBattle'),
            imageUrl: 'shareImage/common-2.png',
            query: 'queryType=friendBattle',
            success: function (res) {
                console.log('SHARE-MENU:', res);
            }
        });

        GameSdkMan.on('group-ranking', () => {
            game.windowManager.clearWindows();

            this.scheduleOnce(() => {
                // ?May open window before all windows destroyed on iOS
                game.windowManager.open(game.Panel.RANK, (windowNode) => {
                    windowNode.getComponent('RankPanel').rankType = 'group';
                });
            }, 0);
        }, this);

        game.shareManager.on('shared', function () {

        }, this);
    },
    start() {
        if ( !GameSdkMan.isOptionsChecked() ) {
            GameSdkMan.checkLaunchOptions();
        }

        this.musicToggle.isChecked = !game.player.enableSoundEffect;

        if (game.player.hasDailyPrize) {
            game.player.itemCount++;
            game.player.shareCount = this.shareCount;

            game.player.hasDailyPrize = false;
        }
    },
    onDestroy() {
        game.shareManager.targetOff(this);
    },

    startGameWithShield() {
        if (game.player.itemCount == 0) {
            game.windowManager.open(game.Panel.SHARE_REWARD, (windowNode) => {
                let shareMan = windowNode.getComponent('ShareManager');
                shareMan.setChecker((type, callback) => {
                    if (game.player.shareCount == 0) {
                        game.windowManager.open(game.Panel.SHARE_LIMITED_BOX, (windowNode) => {
                            windowNode.on('destroy', callback);
                        });
                        return false;
                    }
                    return true;
                });
                shareMan.on('shared', () => {
                    windowNode.destroy();

                    if (game.player.shareCount == 0) return;

                    game.player.itemCount++;
                    game.player.shareCount--;

                    game.player.isShieldMode = true;
                    cc.director.loadScene('battle');
                });
            });
            return;
        }

        game.player.isShieldMode = true;
        cc.director.loadScene('battle');
    }
});