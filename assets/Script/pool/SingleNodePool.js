
let Pool = cc.Class({
    extends: cc.NodePool,

    ctor() {
        this.prefabNode = null;
    },

    get() {
        if (this.size() > 0)
            return this._super();

        return cc.instantiate(this.prefabNode);
    },
    put(node) {
        this._super(node);
    },

    setPrefabNode(prefabNode) {
        this.prefabNode = prefabNode;
    }
});

module.exports = Pool;