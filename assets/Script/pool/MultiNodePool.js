let Pool = function (poolHandlerComp) {
    this.poolHandlerComp = poolHandlerComp;

    this.prefabNodeMap = {};
    this.poolMap = {};
};

Pool.prototype = {
    constructor: Pool,

    get(type) {
        let pool = this.poolMap[type];
        if (!pool) cc.error('pool not found:', type);
        // cc.log('--MP-GET:', this.poolHandlerComp, Object.values(this.poolMap).map(pool => pool.size()));

        if (pool.size() > 0)
            return pool.get();

        return cc.instantiate(this.prefabNodeMap[type]);
    },
    put(node) {
        let comp = node.getComponent(this.poolHandlerComp);
        if (!comp) cc.error('component not found:', node, this.poolHandlerComp);
        let type = comp.type;

        let pool = this.poolMap[type];
        pool.put(node);
    },

    setPrefabNodeMap(prefabNodeMap) {
        this.prefabNodeMap = prefabNodeMap;

        for (let i = 0, keys = Object.keys(prefabNodeMap), key; key = keys[i++];) {
            this.poolMap[key] = new cc.NodePool(this.poolHandlerComp);
        }
    }
};

module.exports = Pool;