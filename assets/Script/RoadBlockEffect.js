
let RoadBlockEffectPool = require('GameObjectPool').RoadBlockEffect;
const RoadBlock = require('RoadBlock');


cc.Class({
    extends: cc.Component,

    properties: {
        blockEffectNode: cc.Node,
        bigBlockEffectNode: cc.Node
    },

    ctor() {
        this.effectNode = null;
        this.color = null;
    },
    onLoad() {
        this.blockEffectNode.active = false;
        this.bigBlockEffectNode.active = false;

        this.effectNode.active = true;
    },

    unuse() {
        if (this.effectNode) {
            this.effectNode.active = false;
            this.getParticleSystem().resetSystem();
        }
    },
    reuse() {

    },

    removeSelf() {
        RoadBlockEffectPool.put(this.node);
    },

    setBlockType(type) {
        this.effectNode = type == RoadBlock.Type.BIG ? this.bigBlockEffectNode : this.blockEffectNode;
        this.effectNode.active = true;

        let particleSystem = this.getParticleSystem();
        particleSystem.startColor = this.color;
        particleSystem.endColor = cc.color(this.color.r, this.color.g, this.color.b, 99);
    },

    getParticleSystem() {
        return this.effectNode.getComponent(cc.ParticleSystem);
    }
});