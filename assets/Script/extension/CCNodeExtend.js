
/* Node */
cc.Node.prototype.getPositionToWorld = function (anchor = cc.p(0, 0)) {
    return this.convertToWorldSpaceAR( cc.p(this.width * anchor.x, this.height * anchor.y) );
};


/* Sprite */
cc.Sprite.prototype.loadTexture = function (texURL, callback) {
    cc.loader.loadRes(texURL, cc.SpriteFrame, (err, spriteFrame) => {
        if (err) {
            cc.error(`Load texture error: ${texURL}, ${err}`);
            return;
        }

        if (!cc.isValid(this)) return;

        this.spriteFrame = spriteFrame;

        callback && callback();
    });
};
