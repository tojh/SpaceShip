
let Player = require('Player');
let WindowManager = require('WindowManager');
let ShareManager = require('ShareManager');
let BattleManager = require('BattleManager');


let game = {
    player: new Player(),
    windowManager: new WindowManager(),
    shareManager: new ShareManager(),
    battleManager: new BattleManager()
};

game.Config = {
    DEBUG: 2    // 0 - disable, 1 - enable, 2 - default
};


// Resource
const RoadBlock = require('RoadBlock');
game.RoadBlock = new Map([
    [RoadBlock.Type.SMALL, 'diji1'],
    [RoadBlock.Type.MEDIUM, 'diji2'],
    [RoadBlock.Type.BIG, 'diji3']
]);

const Item = require('Item');
game.Item = new Map([
    [Item.Type.BULLET_DAMAGE, 'daoju1'],
    [Item.Type.SHOOT_SPEED, 'daoju2'],
    [Item.Type.BULLET_NUM, 'daoju3']
]);

game.Effect = {
    ROADBLOCK_REMOVED: 'color'
};


game.Panel = {
    GAME_RESULT: 'gameOver',
    CONTINUE_PLAY: 'deifen',
    SHARE_REWARD: 'hudunkai',
    SHARE_LIMITED_BOX: 'fenxiangshangxian',
    RANK: 'qunpaihang'
};


game.Audio = {
    BUFF: 'BUFF',
    BULLET: 'zidan',
    BLOCK_ATTACKED: 'jizhong',
    BLOCK_DIED: 'JIPO'
};


window.game = game;