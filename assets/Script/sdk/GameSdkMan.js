

let SdkMan = cc.Class({
    extends: cc.EventTarget,

    properties: {
        domain: 'https://download.qlgame.net/',
        folder: 'spaceship/'
    },

    ctor () {
        this.shareTicket = '';
        this.shareMenuOptions = {};

        this.sharedCanvasTexture = new cc.Texture2D();
    },

    init () {
        if (CC_WECHATGAME) {
            this.postMsg({message: 'checkOpenId'});  // init user openId
            this.postMsg({message: 'checkData'});  // init user cloudStorage data

            this.initShareMenu();


            if (game.Config.DEBUG >> 1) {
                // default
            } else
                wx.setEnableDebug({enableDebug: !!game.Config.DEBUG});

            wx.onMemoryWarning(function (res) {
                console.log("** MEM-warning:", JSON.stringify(res));
            });

        }
    },
    initShareMenu () {
        wx.showShareMenu({
            withShareTicket: true,
            success (res) {

            }
        });

        wx.onShareAppMessage((res) => {
            return this.shareMenuOptions;
        });
    },

    /**
     * 发送消息到子域
     */
    postMsg (msg) {
        if (CC_WECHATGAME) {
            let openDataContext = wx.getOpenDataContext();
            openDataContext.postMessage(msg);
        }
    },
    /**
     * 打开分享界面
     */
    shareAppMsg(options) {
        if (CC_WECHATGAME) {
            options.imageUrl = this.domain + this.folder + options.imageUrl;
            wx.shareAppMessage(options);
        }
    },
    setShareMenuOptions(options) {
        options.imageUrl = this.domain + this.folder + options.imageUrl;
        this.shareMenuOptions = options;
    },

    getSharedCanvasSpriteFrame () {
        if (CC_WECHATGAME) {
            this.sharedCanvasTexture.initWithElement(sharedCanvas);
            this.sharedCanvasTexture.handleLoadedTexture();
        }

        return new cc.SpriteFrame(this.sharedCanvasTexture);
    },

    checkLaunchOptions() {
        if (CC_WECHATGAME) {
            let opts = wx.getLaunchOptionsSync();
            this.checkShare(opts);

            wx.onShow((res) => {
                // 若玩家返回微信点击其它群分享链接需重新检测
                this.checkShare(res);
            });
            wx.onHide(() => {

            });
        }
    },
    checkShare(data) {
        if (data.query.queryType == 'friendBattle') {
            this.emit('friend-battle');

        } else if(data.query.queryType == 'groupRanking') {

        }

        if (data.shareTicket) {
            // 验证群分享 shareTicket 有效 (shareTicket always changed)
            wx.getShareInfo({
                shareTicket: data.shareTicket,
                success: (res) => {
                    this.shareTicket = data.shareTicket;

                    this.emit('group-ranking');
                },
                fail: () => {
                    console.log('Group shareTicket invalid');
                }
            });
        } else {
            // console.error('Group share data invalid');
        }
    },

    isOptionsChecked() {
        return !!this.shareTicket;
    }
});


module.exports = new SdkMan();
