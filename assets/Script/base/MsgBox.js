

cc.Class({
    extends: cc.Component,

    properties: {
        okButton: cc.Button
    },

    confirm() {
        this.node.destroy();

        this.node.emit('confirm');
    }
});
