
let BaseComponent = cc.Class({
    extends: cc.Component,

    properties: {},

    unuse() {

    },
    reuse() {
        this.node.off('removed');
    },

    removeSelf() {
        // put to pool
        // this.removeNodeEvents();

        this.node.emit('removed');
    },

    removeNodeEvents() {
        this.node.off('position-changed');
        this.node.off('size-changed');
        this.node.off('anchor-changed');
    }
});

let props = ['x', 'y', 'position', 'anchorX', 'anchorY', 'scale', 'zIndex', 'width', 'height'];
for (let prop of props) {
    cc.js.getset(
        BaseComponent.prototype,
        prop,
        function () { return this.node[prop]; },
        function (val) { this.node[prop] = val; }
    );
}
