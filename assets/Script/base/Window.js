

cc.Class({
    extends: cc.Component,
    properties: {
        bg: cc.Sprite,
        windowBg: cc.Sprite,
        closeButton: cc.Button,

        relativeNode: cc.Node,

        tapBgToClose: false
    },

    onLoad() {
        let handler = event => event.stopPropagation();

        this.bg.node.on(cc.Node.EventType.TOUCH_START, (event) => {
            this.tapBgToClose && this.node.destroy();
            handler(event);
        });
        this.bg.node.on(cc.Node.EventType.TOUCH_MOVE, handler);
        this.bg.node.on(cc.Node.EventType.TOUCH_END, handler);

        this.windowBg.node.on(cc.Node.EventType.TOUCH_START, handler);
        this.windowBg.node.on(cc.Node.EventType.TOUCH_MOVE, handler);
        this.windowBg.node.on(cc.Node.EventType.TOUCH_END, handler);

        this.node.position = cc.pNeg(this.relativeNode.position);
    },
    onDestroy() {
        this.node.emit('destroy');
    },

    destroyWindow() {
        this.node.destroy();
    },

    tapEmitEvent(event, data) {
        this.node.emit(data);
    }
});