
let ItemPool = require('GameObjectPool').Item;


const Type = cc.Enum({
    BULLET_DAMAGE: 1,
    SHOOT_SPEED: 2,
    BULLET_NUM: 3
});


let Item = cc.Class({
    extends: require('BaseComponent'),

    properties: {
        type: {
            default: Type.BULLET_DAMAGE,
            type: Type
        }
    },

    onLoad() {

    },

    removeSelf() {
        ItemPool.put(this.node);

        this._super();
    }
});

Item.Type = Type;