
const GameSdkMan = require("GameSdkMan");


cc.Class({
    extends: cc.Component,

    properties: {
        rank: cc.Sprite
    },

    onLoad() {

    },
    start() {
        GameSdkMan.postMsg({message: 'aroundRank', isTitle: false});
    },
    update() {
        this.rank.spriteFrame = GameSdkMan.getSharedCanvasSpriteFrame();
    }
});
