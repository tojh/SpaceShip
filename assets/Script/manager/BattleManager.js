

cc.Class({
    extends: cc.Component,
    mixins: [cc.EventTarget],

    properties: {

    },

    emitEvent(message, detail) {
        this.emit(message, detail);

        game.battleManager.emit(message, detail);
    },

    pause() {
        this.emitEvent('pause');
    },
    resume() {
        this.emitEvent('resume');
    },
    end() {
        this.emitEvent('end');
    },

    restart() {
        this.emitEvent('restart');
    },
    retry() {
        this.emitEvent('retry');
    },

    save() {
        this.emitEvent('save');
    },
    restore() {
        this.emitEvent('restore');
    }
});
