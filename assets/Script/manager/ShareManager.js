
const GameSdkMan = require("GameSdkMan");
const i18n = require('i18n');


cc.Class({
    extends: cc.Component,
    mixins: [cc.EventTarget],

    properties: {

    },

    onLoad() {
        this.checker = (type, callback) => true;  // check valid
        this.shareOptions = {};
    },

    emitEvent(message, detail) {
        this.emit(message, detail);

        game.shareManager.emit(message, detail);
    },

    shareToGroup(titleKey) {
        this.shareOptions = {
            title: i18n.t(titleKey),
            imageUrl: 'shareImage/common-1.png',
            query: 'queryType=groupRanking',
            success: (res) => {
                console.log('SHARE-G:', res, cc.isValid(this));

                if (res.shareTickets)
                    this.emitEvent('shared', this.node);
                else
                    this.emitEvent('share_type_failed', this.node);
            }
        };

        if ( !this.checker('group', this.share.bind(this)) ) return;

        this.share();
    },
    shareToFriend(titleKey) {
        this.shareOptions = {
            title: i18n.t(titleKey),
            imageUrl: 'shareImage/common-2.png',
            query: 'queryType=friendBattle',
            success: (res) => {
                console.log('SHARE-F:', res, cc.isValid(this));

                // if (!res.shareTickets)
                this.emitEvent('shared', this.node);
            }
        };

        if ( !this.checker('friend', this.share.bind(this)) ) return;

        this.share();
    },

    share() {
        GameSdkMan.shareAppMsg(this.shareOptions);
        // this.emitEvent('shared', this.node);
    },

    tapShareToGroup(event, data) {
        this.shareToGroup(data);
    },
    tapShareToFriend(event, data) {
        this.shareToFriend(data);
    },

    setChecker(fn) {
        this.checker = fn;
    }
});