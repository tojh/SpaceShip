
const DataProperty = require('Player').DataProperty;


cc.Class({
    extends: cc.Component,

    properties: {
        player: {
            default: DataProperty.score,
            type: DataProperty
        }
    },

    onLoad() {
        let label = this.node.getComponent(cc.Label);
        if (label) {
            let prop = DataProperty[ Object.values(DataProperty).indexOf(this.player) + 1 ];
            if (!prop) cc.error('player prop not found:', this.player);

            label.string = game.player[prop];
        }
    }
});