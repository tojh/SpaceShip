
const SingleNodePool = require('SingleNodePool');
const MultiNodePool = require('MultiNodePool');

module.exports = {
    Bullet: new SingleNodePool('Bullet'),
    Item: new MultiNodePool('Item'),
    RoadBlock: new MultiNodePool('RoadBlock'),
    RoadBlockEffect: new SingleNodePool('RoadBlockEffect')
};