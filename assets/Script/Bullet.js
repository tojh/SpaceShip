
let BulletPool = require('GameObjectPool').Bullet;


cc.Class({
    extends: require('BaseComponent'),

    properties: {
        speed: 60,
        damage: 1
    },

    ctor() {
        this.isMoving = false;
    },
    update() {
        if (!this.isMoving) return;

        this.y += this.speed;
    },

    onCollisionEnter(other, self) {

    },
    onCollisionStay() {

    },
    onCollisionExit() {

    },

    unuse() {
        this.isMoving = false;
    },
    reuse() {

    },

    removeSelf() {
        BulletPool.put(this.node);
    },

    move() {
        this.isMoving = true;
    }
});