
const LevelFactory = require('LevelFactory');
const GameSdkMan = require("GameSdkMan");


cc.Class({
    extends: cc.Component,

    properties: {
        stage: require('Stage'),

        scoreLabel: cc.Label,
        rank: cc.Sprite
    },

    onLoad() {
        game.battleManager.on('restart', this.restart, this);
        game.battleManager.on('end', this.gameOver, this);

        let audioMan = this.getComponent('AudioManager');
        this.stage.node.on('score', ({detail: score}) => {
            game.player.score += score;

            this.updateView();
            this.playScoreAnim();

            audioMan.setClipAndPlay(game.Audio.BLOCK_ATTACKED);
        });
        this.stage.node.on('shoot-bullet', () => {
            audioMan.setClipAndPlay(game.Audio.BULLET);
        });
        this.stage.node.on('use-item', () => {
            audioMan.setClipAndPlay(game.Audio.BUFF);
        });
        this.stage.node.on('destroy-block', () => {
            audioMan.setClipAndPlay(game.Audio.BLOCK_DIED);
        });

        cc.game.on(cc.game.EVENT_SHOW, () => {
            if (!this.stage.isEnded) this.stage.scroll();
        }, this);
        cc.game.on(cc.game.EVENT_HIDE, () => {
            this.stage.stopScroll();
        }, this);

        // Player
        game.player.on('set-prop', function ({detail: prop}) {
            switch (prop) {
                case 'score':
                    GameSdkMan.postMsg({message: 'compareRank', score: game.player.score});

                    this.updateView();
                    break;
            }
        }, this);

        this._isRetried = false;


        LevelFactory.init(() => {
            this.init();
        });
    },
    start() {
        game.player.score = 0;

        this.updateView();
    },
    update(dt) {
        this.rank.spriteFrame = GameSdkMan.getSharedCanvasSpriteFrame();
    },
    onDestroy() {
        game.battleManager.targetOff(this);
        cc.game.targetOff(this);
    },

    bindTouchEvents() {
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    },
    unbindTouchEvents() {
        this.node.off(cc.Node.EventType.TOUCH_START);
        this.node.off(cc.Node.EventType.TOUCH_MOVE);
        this.node.off(cc.Node.EventType.TOUCH_END);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL);
    },
    onTouchStart(event) {
        if (game.Config.DEBUG & 1) this.stage.scroll();
    },
    onTouchMove(event) {
        this.stage.airShip.x += event.getDeltaX();
    },
    onTouchEnd() {
        if (game.Config.DEBUG & 1) this.stage.stopScroll();
    },
    onTouchCancel() {
        // this.stage.stopScroll();
    },

    init() {
        let manager = cc.director.getCollisionManager();
        manager.enabled = true;
        // manager.enabledDebugDraw = true;

        this.bindTouchEvents();

        this.stage.init();
        if (!(game.Config.DEBUG & 1)) this.stage.scroll();

        if (game.player.isShieldMode) {
            game.player.itemCount--;

            this.stage.airShip.setShieldActive(true);
            game.player.isShieldMode = false;
        }
    },
    restart() {
        cc.director.getCollisionManager().enabled = true;

        this.bindTouchEvents();

        game.player.score = 0;

        this.updateView();

        this.stage.restart();
        if (!(game.Config.DEBUG & 1)) this.stage.scroll();

        this._isRetried = false;
    },
    retry() {
        cc.director.getCollisionManager().enabled = true;

        this.bindTouchEvents();

        this.stage.retry();
        if (!(game.Config.DEBUG & 1)) this.stage.scroll();

        this._isRetried = true;
    },
    gameOver() {
        cc.log('<<<GAME-OVER>>>');
        return;

        cc.director.getCollisionManager().enabled = false;

        this.unbindTouchEvents();

        this.stage.stopScroll();


        if (this._isRetried) {
            game.windowManager.open(game.Panel.GAME_RESULT);
            return;
        }

        game.windowManager.open(game.Panel.CONTINUE_PLAY, (windowNode) => {
            windowNode.getComponent('ShareManager').on('shared', () => {
                windowNode.destroy();

                this.retry();
            });
        });
    },

    updateView() {
        this.scoreLabel.string = game.player.score;
    },

    playScoreAnim() {
        if (this.scoreLabel.node.getNumberOfRunningActions()) return;

        this.scoreLabel.node.runAction( cc.sequence(
            cc.scaleTo(0.1, 1.2),
            cc.scaleTo(0.1, 1)
        ) );
    }
});
